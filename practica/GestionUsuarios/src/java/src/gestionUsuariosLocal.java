/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import javax.ejb.Local;

/**
 *
 * @author electra
 */
@Local
public interface gestionUsuariosLocal {
    
    public boolean isAbonado(String login);
    public boolean isEmpleado(String login);
    public boolean isPsswdOK(String login, String passwd, String tipoUser);
    public String getNif(String login);
    public boolean addAbonado(String nif, String nombre, String apellidos, 
            String login, String passwd);
    public boolean delAbonado(String nif);
    
}
