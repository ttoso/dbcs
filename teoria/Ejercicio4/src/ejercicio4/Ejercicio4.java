/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;


import ejb.MySession;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 *
 * @author toso
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws NamingException {
        Properties prop = new Properties();  
        prop.setProperty("org.omg.CORBA.ORBInitialHost","localhost"); 
        prop.setProperty("org.omg.CORBA.ORBInitialPort","3700"); 
        Context context = new InitialContext(); 
        
        MySession bean = (MySession)context.lookup("java:global/HelloWorld/HelloWorld-ejb/MySession"); 
        bean.getResult();
    }
    
}
