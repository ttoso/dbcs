/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package holamundo;

import javax.ejb.Local;

/**
 *
 * @author electra
 */
@Local
public interface HolaMundoSessionBeanLocal {

    String sayHi(String name);
    
}
