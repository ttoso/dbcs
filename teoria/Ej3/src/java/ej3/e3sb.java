/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej3;

import javax.ejb.Stateless;

/**
 *
 * @author toso
 */
@Stateless
public class e3sb implements e3sbLocal {

    @Override
    public double kmtomiles(int km) {
        return km*0.621;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
